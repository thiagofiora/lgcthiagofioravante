﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace LGCThiago.Src.PageObjects.Widgets
{
    class SearchResultLeftPanelWidget : BaseWidget
    {
        IWebElement CityTitle => Driver.FindElement(By.CssSelector(".section-hero-header-title-description div h1"));

        IWebElement BtnDirections => Driver.FindElement(By.XPath("//div[@data-value='Directions' or @data-value='Rotas']"));

        public SearchResultLeftPanelWidget(IWebDriver driver) : base(driver) { }

        public override void WaitElements()
        {
            Wait.Until(el => CityTitle.Text != string.Empty);
            Wait.Until(el => BtnDirections.Displayed && BtnDirections.Enabled);
        }

        public string GetTitleText()
        {            
            return CityTitle.Text;
        }

        public void Directions()
        {
            BtnDirections.Click();
        }
    }
}
