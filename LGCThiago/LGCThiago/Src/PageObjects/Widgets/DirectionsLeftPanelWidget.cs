﻿using OpenQA.Selenium;

namespace LGCThiago.Src.PageObjects.Widgets
{
    class DirectionsLeftPanelWidget : BaseWidget
    {
        IWebElement DirectionsFrom => Driver.FindElement(By.CssSelector("#directions-searchbox-1 input"));

        IWebElement DirectionsTo => Driver.FindElement(By.CssSelector("#directions-searchbox-1 input"));

        public DirectionsLeftPanelWidget(IWebDriver driver) : base(driver) { }

        public override void WaitElements()
        {
            Wait.Until(el => DirectionsFrom.Displayed && DirectionsFrom.Enabled);
            Wait.Until(el => DirectionsTo.Displayed && DirectionsTo.Enabled);
        }

        public string GetCityFromDirections()
        {
            return DirectionsFrom.Text;
        }

        public string GetCityToDirections()
        {
            return DirectionsTo.GetAttribute("aria-label");
        }
    }
}
