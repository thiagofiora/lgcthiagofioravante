﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace LGCThiago.Src.PageObjects.Widgets
{
    public abstract class BaseWidget
    {
        protected string PageUri;

        protected IWebDriver Driver;
        protected WebDriverWait Wait;

        public BaseWidget(IWebDriver driver)
        {
            this.Driver = driver;

            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            WaitElements();
        }

        public abstract void WaitElements();

    }
}
