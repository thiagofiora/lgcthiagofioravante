﻿using LGCThiago.Src.PageObjects.Widgets;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace LGCThiago.Src.PageObjects
{
    class GoogleMap
    {
        private string PageUri = "https://www.google.com/maps";

        private IWebElement SearchBox => Driver.FindElement(By.Id("searchboxinput"));

        private SearchResultLeftPanelWidget SearchPanel;
        private DirectionsLeftPanelWidget directionsPanel;

        protected IWebDriver Driver;
        protected WebDriverWait Wait;

        public GoogleMap(IWebDriver driver) 
        {
            this.Driver = driver;

            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public void WaitElements()
        {
            Wait.Until(el => SearchBox.Displayed && SearchBox.Enabled);
        }

        public void GoToPage()
        {
            Driver.Navigate().GoToUrl(PageUri);
            WaitElements();
        }

        public void Search(string text)
        {
            SearchBox.SendKeys(text + Keys.Enter);
            SearchPanel = new SearchResultLeftPanelWidget(Driver);
        }

        public string GetSearchCityText()
        {
            return SearchPanel.GetTitleText();
        }

        public void ClickDirections()
        {
            SearchPanel.Directions();
            directionsPanel = new DirectionsLeftPanelWidget(Driver);
        }

        public string GetCityFromDirections()
        {
            return directionsPanel.GetCityFromDirections();
        }

        public string GetCityToDirections()
        {
            return directionsPanel.GetCityToDirections();
        }
    }
}
