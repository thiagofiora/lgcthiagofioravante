using LGCThiago.Src.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace LGCThiago.Test
{
    [TestFixture("chrome")]
    [TestFixture("firefox")]
    public class GoogleMapTests
    {
        private string Browser;
        public IWebDriver Driver;

        public GoogleMapTests(string browser)
        {
            this.Browser = browser;
        }

        [SetUp]
        public void Setup()
        {
            switch (Browser)
            {
                case "firefox":
                    var caps = new FirefoxOptions();
                    caps.AcceptInsecureCertificates = true;
                    Driver = new FirefoxDriver(caps);
                    break;
                case "chrome":                    
                default:
                    Driver = new ChromeDriver();
                    break;
            }
            Driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                Driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        [Test]
        public void TestDirectionsToDublin()
        {
            GoogleMap map = new GoogleMap(Driver);
            map.GoToPage();
            map.Search("Dublin");
            Assert.AreEqual("Dublin", map.GetSearchCityText());

            map.ClickDirections();
            Assert.IsTrue(string.IsNullOrEmpty(map.GetCityFromDirections()));
            Assert.IsTrue(map.GetCityToDirections().Contains("Dublin, Irlanda"));
        }
    }
}